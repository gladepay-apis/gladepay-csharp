﻿using System;

namespace Gladepay
{
    public class Api
    {
        private readonly string _merchant_id;
        private readonly string _merchant_key;

        public Api(string merchant_id, string merchant_key)
        {
            _merchant_id = merchant_id;
            _merchant_key = merchant_key;
        }


        /// <summary>This method stores the merchant credentials as the default credentials.
        /// <example>For example:
        /// <code>
        ///    Api.Init(YOUR_GLADEPAY_MERCHANT_ID, YOUR_GLADEPAY_MERCHANT_KEY);
        /// </code>
        /// </example>
        /// </summary>
        public static void Init(string merchant_id, string merchant_key)
        {
            Container.SetDefaultApi(merchant_id, merchant_key);
        }

        internal static Api GetDefaultApi()
        {
            //Get the default api 
            return Container.Instance.DefaultApi;
        }

        public string MerchantId => _merchant_id;
        public string MerchantKey => _merchant_key;
        public string RootUrl => "https://demo.api.gladepay.com";


     

    }
}
