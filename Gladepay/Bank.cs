﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay
{
    public class Bank
    {
        private static readonly Api _api = Api.GetDefaultApi();
        private static RestClient client;

        public Bank(string name, string code)
        {
            Name = name;
            Code = code;
        }

        public Bank(string code)
        {
            Code = code;
        }

        public string Name { get; set; }
        public string Code { get; set; }

        public override string ToString()
        {
            return $"{Name}  {Code}";
        }

        public Response VerifyAccountName(string accountNumber, Api api=null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From Fetch verify accunt name");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("resources", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["inquire"] = "accountname",
                ["accountnumber"] = accountNumber,
                ["bankcode"] = Code
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End verify account name");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var message = (string)json["status"];
            return new Response(status, message, json);
        }

        public static IList<Bank> ChargeableBanks(Api api=null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From Fetch Chargeable banks");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("resources", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["inquire"] = "supported_chargable_banks"
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End get chargeable banks");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var result = new List<Bank>();
            if (((string)json["status"]).Equals("success"))
            {
                var banks = json["data"] as JArray;
                foreach(var bank in banks)
                {
                    var bank_obj = bank as JObject;
                    var name = (string)bank_obj["bankname"];
                    var code = (string)bank_obj["bankcode"];
                    var b = new Bank(name, code);
                    Console.WriteLine(b.ToString());
                    result.Add(b);
                }
            }
            return result;
        }


        public static IList<Bank> AllBanks(Api api=null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From Fetch Chargeable banks");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("resources", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["inquire"] = "banks"
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End get chargeable banks");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var result = new List<Bank>();
            if (((string)json["status"]).Equals("success"))
            {
                var banks = json["data"] as JArray;
                foreach (var bank in banks)
                {
                    var bank_obj = bank as JObject;
                    var name = (string)bank_obj["bankname"];
                    var code = (string)bank_obj["bankcode"];
                    result.Add(new Bank(name, code));
                }
            }
            return result;
        }


        public static Response VerifyBvnNumber(string bvn, Api api=null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From verify bvn number");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("resources", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["inquire"] = "bvn",
                ["bvn"] = bvn
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End verify bvn");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var message = (string)json["status"];
            return new Response(status, message, json);
        }
    }
}
