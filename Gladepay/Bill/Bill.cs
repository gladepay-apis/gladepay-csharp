﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay.Bill
{
    public class Bill
    {
        private static readonly Api _api = Api.GetDefaultApi();
        private static RestClient client;
        private static IList<BillCategory> _categories;
        private static IList<BillSection> _sections;

        internal Bill(string name, double amount, double discount, double fee, 
            string paycode, bool requireNameQuery, BillSection section)
        {
            Name = name;
            Amount = amount;
            Discount = discount;
            Fee = fee;
            PayCode = paycode;
            RequireNameQuery = requireNameQuery;
            Section = section;
        }

        public string Name { get; set; }
        public double Amount { get; set; }
        public double Discount { get; set; }
        public double Fee { get; set; }
        public string PayCode { get; set; }
        public bool RequireNameQuery { get; set; }
        public BillSection Section { get; set; }


        public static IList<Bill> Fetch(Api api = null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From fetch bills");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("bills", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "pull"
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End fetch bills");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var bill_data = json["data"] as JObject;
            var bill_categories = bill_data["categories"] as JArray;
            var bill_sections = bill_data["bills"] as JArray;
            var bill_items = bill_data["items"] as JArray;
            _categories = new List<BillCategory>();
            _sections = new List<BillSection>();
            var bills = new List<Bill>();
            foreach(var i in bill_categories){ _categories.Add(ParseBillCategory(i as JObject)); }
            foreach(var i in bill_sections) { _sections.Add(ParseBillSection(i as JObject)); }
            foreach(var i in bill_items) { bills.Add(ParseBill(i as JObject)); }
            return bills;
            
        }

        public static Response VerifyPayment(string txnRef, Api api=null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From Verify bill payment");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("bills", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "verify",
                ["txnRef"] = txnRef
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End verify bill payment");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            return new Response(status, code, json);
        }

        public Response Pay(double amount, string reference, string orderRef, Api api = null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From Bill payment");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("bills", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "pay",
                ["paycode"] = PayCode,
                ["reference"] = reference,
                ["amount"] = amount,
                ["orderRef"] = orderRef
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End bill payment");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            return new Response(status, code, json);
        }

        public Response ResolveName(double amount, string reference, Api api = null)
        {
            if (!RequireNameQuery)
            {
                throw new Exception("This bill does not require resolvement");
            }
            var mApi = api ?? _api;
            Console.WriteLine("From Resolve customer name");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("bills", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "resolve",
                ["paycode"] = PayCode,
                ["reference"] = reference,
                ["amount"] = amount
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End resolve customer name");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            return new Response(status, code, json);
        }

        private static BillCategory ParseBillCategory(JObject category)
        {
            var name = (string)category["name"];
            var code = (string)category["code"];
            return new BillCategory(name, code);
        }

        private static BillSection ParseBillSection(JObject section)
        {
            var id = (int)section["id"];
            var name = (string)section["name"];
            var reference = (string)section["reference"];
            var categoryName = (string)section["category"];
            var category = GetBillCategoryByName(categoryName);
            return new BillSection(id, name, reference, category);
        }

        private static Bill ParseBill(JObject bill)
        {
            var name = (string)bill["name"];
            var amount = (double)bill["amount"];
            var discount = (double)bill["discount"];
            var fee = (double)bill["fee"];
            var paycode = (string)bill["paycode"];
            var rqn = (int)bill["require_name_query"];
            var section_id = (int)bill["bills_id"];
            var section = GetBillSection(section_id);
            return new Bill(name, amount, discount, fee, paycode, rqn == 1, section);
        }

        private static BillCategory GetBillCategoryByName(string name)
        {
            foreach(var category in _categories)
            {
                if (category.Name.Equals(name)) { return category; }
            }
            return null;
        }

        private static BillSection GetBillSection(int id)
        {
            foreach (var section in _sections)
            {
                if (section.Id == id) { return section; }
            }
            return null;
        }
    }
}
