﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay.Bill
{
    public class BillCategory
    {
        internal BillCategory(string name, string code)
        {
            Name = name;
            Code = code;
        }

        public string Name { get; set; }
        public string Code { get; set; }
    }
}
