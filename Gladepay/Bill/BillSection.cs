﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay.Bill
{
    public class BillSection
    {
        internal BillSection(int id, string name, string reference, BillCategory category)
        {
            Id = id;
            Name = name;
            Reference = reference;
            Category = category;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Reference { get; set; }
        public BillCategory Category { get; set; }
    }
}
