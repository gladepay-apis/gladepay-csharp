﻿using Gladepay.Models;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay
{
    public class Client
    {
        private static readonly Api _api = Api.GetDefaultApi();
        private static RestClient client;

        internal Client(string firstname, string lastname, string middlename, string email, string phonenumber,
            ClientGender? gender, DateTime? dateOfBirth, string bvn, BankAccount bank, IList<Address> addresses,
            JToken meta)
        {
            FirstName = firstname;
            LastName = lastname;
            MiddleName = middlename;
            Email = email;
            Gender = gender;
            DateOfBirth = dateOfBirth;
            PhoneNumber = phonenumber;
            BVN = bvn;
            BankAccount = bank;
            Addresses = addresses;
            Meta = meta;
        }

        internal Client(string firstname, string lastname, string middlename, string email, string phonenumber,
            ClientGender? gender, DateTime dateOfBirth, string bvn, BankAccount bank, Address address,
            JObject meta):this(firstname,lastname,middlename,email,phonenumber,gender,dateOfBirth,bvn,
                bank,new List<Address>() { address },meta){}

        internal Client(string firstname, string lastname, string email, string bvn, string phonenumber)
        {
            FirstName = firstname;
            LastName = lastname;
            Email = email;
            BVN = bvn;
            PhoneNumber = phonenumber;
        }

        internal Client()
        {

        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string Email { get; set; }
        public ClientGender? Gender { get; set; }
        public ClientStatus Status { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string PhoneNumber { get; set; }
        public string BVN { get; set; }
        public BankAccount BankAccount { get; set; }
        public IList<Address> Addresses { get; set; }
        public JToken Meta { get; set; }
        public DateTime CreatedAt { get; set; }
        public int Id { get; set; }


        public static IList<Client> Fetch(Api api=null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From fetch clients");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("clients", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "list"
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            var result = new List<Client>();

            Console.WriteLine(response.Content);
            Console.WriteLine("End fetch clients");
            var stats = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            if (code == 200)
            {
                var clients_data = json["data"] as JArray;
                foreach(var cdata in clients_data)
                {
                    var client_obj = cdata as JObject;
                    result.Add(Parse(client_obj));
                }
                return result;
            }
            throw new Exception("Unable to fetch clients");
        }


        public static Client Create(string firstname, string lastname, string email, string phonenumber,
            string bvn, string middlename = null, ClientGender? gender = null,
            DateTime? dateOfBirth = null,  BankAccount bank = null, IList<Address> addresses = null,
            ClientStatus status=ClientStatus.ACTIVE, JToken meta = null, bool verify_bvn=false, Api api = null)
        {
            var mApi = api ?? _api;
            var new_client = new Client();
            new_client.FirstName = firstname;
            new_client.LastName = lastname;
            new_client.MiddleName = middlename;
            new_client.Email = email;
            new_client.Gender = gender;
            new_client.DateOfBirth = dateOfBirth;
            new_client.PhoneNumber = phonenumber;
            new_client.BVN = bvn;
            new_client.BankAccount = bank;
            new_client.Addresses = addresses;
            new_client.Meta = meta;
            Console.WriteLine("From new client");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("clients", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "create",
                ["data"] = new_client.ToJson(true),
                ["verify_bvn"] = verify_bvn,
                ["status"] = status.ToString().ToLower()
            };
            Console.WriteLine(data.ToString());
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End new client");
            var stats = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            if (code == 200)
            {
                var client_data = json["data"] as JObject;
                return Parse(client_data);
            }
            return null;
        }


        public static Client Update(int id, string firstname=null, string lastname=null, string middlename=null,
            string email=null, string phonenumber=null,ClientGender? gender=null, 
            DateTime? dateOfBirth=null, string bvn=null, BankAccount bank=null, IList<Address> addresses=null,
            JToken meta=null, Api api=null)
        {
            var mApi = api ?? _api;
            var update_client = new Client();
            update_client.FirstName = firstname;
            update_client.LastName = lastname;
            update_client.MiddleName = middlename;
            update_client.Email = email;
            update_client.Gender = gender;
            update_client.DateOfBirth = dateOfBirth;
            update_client.PhoneNumber = phonenumber;
            update_client.BVN = bvn;
            update_client.BankAccount = bank;
            update_client.Addresses = addresses;
            update_client.Meta = meta;
            Console.WriteLine("From update client");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("clients", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "update",
                ["client_id"] = id,
                ["data"] = update_client.ToJson()
            };
            Console.WriteLine(data.ToString());
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End update client");
            var stats = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            if (code == 200)
            {
                var client_data = json["data"] as JObject;
                return Parse(client_data);
            }
            return null;
        }


        public static Client Get(int id, Api api = null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From get client");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("clients", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "view",
                ["client_id"] = id
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End get client");
            var stats = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            if (code == 200)
            {
                var client_data = json["data"] as JObject;
                return Parse(client_data);
            }
            return null;
        }

        public static Client Parse(JObject c)
        {
            DateTime? dob;
            List<Address> addresses;
            ClientGender? gender;
            var id = (int)c["id"];
            var firstname = (string)c["firstname"];
            var lastname = (string)c["lastname"];
            var middlename = c["middlename"].Type == JTokenType.Null ? null: (string)c["middlename"];
            var email = (string)c["email"];
            var phone = (string)c["phone"];
            var bvn = (string)c["bvn"];
            var created_at = DateTime.Parse((string)c["created_at"]);
            var meta = c["meta"].Type == JTokenType.Null ? null : c["meta"];
            var status = StatusFromString((string)c["status"]);
            var bank = c["bankAccount"].Type == JTokenType.Null ? null : BankAccount.Parse(c["bankAccount"] as JObject);
            if(c["gender"].Type == JTokenType.Null || string.IsNullOrEmpty((string)c["gender"])) { gender = null; }
            else { gender = GenderFromString((string)c["gender"]); }
            if(c["dob"].Type == JTokenType.Null || string.IsNullOrEmpty((string)c["dob"])) { dob = null; }
            else { dob = DateTime.Parse((string)c["dob"]); }
            if(c["addresses"].Type == JTokenType.Null) { addresses = null; }
            else
            {
                addresses = new List<Address>();
                var address_list = c["addresses"] as JArray;
                foreach(var address_data in address_list)
                {
                    var address = address_data as JObject;
                    addresses.Add(Address.Parse(address));
                }
            }

            var cli =  new Client(firstname, lastname, middlename, email, phone, gender, dob, bvn, bank, addresses, meta)
            {
                Id = id,
                CreatedAt = created_at,
                Status = status
            };
            Console.WriteLine("From parse client");
            Console.WriteLine(cli.ToJson().ToString());
            Console.WriteLine("End parse client");
            return cli;
        }

        public class Address
        {
            public Address(string line1, string line2, string city, string region,
                string post_code, string country)
            {
                Line1 = line1;
                Line2 = line2;
                City = city;
                Region = region;
                PostCode = post_code;
                Country = country;
            }

            public string Line1 { get; set; }
            public string Line2 { get; set; }
            public string City { get; set; }
            public string Region { get; set; }
            public string PostCode { get; set; }
            public string Country { get; set; }

            public static Address Parse(JObject address)
            {
                return new Address(
                    (string)address["line1"],
                    (string)address["line2"],
                    (string)address["city"],
                    (string)address["region"],
                    (string)address["postcode"],
                    (string)address["country"]
                    );
            }


            public JToken ToJSon()
            {
                var result = new JObject
                {
                    ["line1"] = Line1,
                    ["line2"] = Line2,
                    ["city"] = City,
                    ["region"] = Region,
                    ["postcode"] = PostCode,
                    ["country"] = Country
                };
                Console.WriteLine("Address to string");
                Console.WriteLine(result.ToString());
                Console.WriteLine("End address to string");
                return result;
            }
        }


        public JToken AddressesToJson()
        {
            var array = new JArray();
            if (Addresses != null)
            {
                foreach (var address in Addresses)
                {
                    array.Add(address.ToJSon());
                }
            }
            Console.WriteLine("Addresses To Json");
            Console.WriteLine(array.ToString());
            Console.WriteLine("End addresses to json");
            return array;
        }


        public JToken ToJson(bool isCreate=false)
        {
            var result = new JObject
            {
                ["firstname"] = FirstName,
                ["lastname"] = LastName,
                ["middlename"] = MiddleName,
                ["email"] = Email,
                ["gender"] = Gender?.ToString().ToLower(),
                ["dob"] = DateOfBirth?.ToString("dd-MM-yyyy"),
                ["phone"] = PhoneNumber,
                ["bvn"] = BVN,
                ["addresses"] = AddressesToJson(),
                ["bankAccount"] = BankAccount == null ? null : BankAccount.ToJson(),
                ["meta"] = Meta
            };
            if (isCreate)return result;
            var keysToRemove = new List<string>();
            Console.WriteLine("Client to json");
            Console.WriteLine(result.ToString());
            Console.WriteLine("End client to json");
            Console.WriteLine("keys to remove from client data");
            foreach (var i in result)
            {
                Console.WriteLine($"list {i.Key}::: {i.Value.Type.ToString()}");
                if (i.Value.Type == JTokenType.Null)
                {
                    Console.WriteLine(i.Key);
                    keysToRemove.Add(i.Key);
                }
                if (i.Value.Type == JTokenType.String && string.IsNullOrEmpty((string)i.Value))
                {
                    Console.WriteLine(i.Key);
                    keysToRemove.Add(i.Key);
                }
                if (i.Value.Type == JTokenType.Array && ((JArray)i.Value).Count <= 0)
                {
                    Console.WriteLine(i.Key);
                    keysToRemove.Add(i.Key);
                }
            }
            Console.WriteLine("end keys to remove");
            foreach (var j in keysToRemove)
            {
                result.Remove(j);
            }
            return result;
        }


        public static ClientStatus StatusFromString(string value)
        {
            if (value.Equals("blacklisted")) return ClientStatus.BLACKLISTED;
            if (value.Equals("active")) return ClientStatus.ACTIVE;
            if (value.Equals("inactive")) return ClientStatus.INACTIVE;
            if (value.Equals("pending_approval")) return ClientStatus.PENDING_APPROVAL;
            if (value.Equals("rejected")) return ClientStatus.REJECTED;
            throw new Exception($"Invalid client status value::: {value}");
        }

        public enum ClientGender
        {
            Male,
            Female
        }

        public static ClientGender GenderFromString(string value)
        {
            if (value.ToLower().Equals("male")) { return ClientGender.Male; }
            if (value.ToLower().Equals("female")) { return ClientGender.Female; }
            if (value.ToLower().Equals("f")) { return ClientGender.Female; }
            if (value.ToLower().Equals("m")) { return ClientGender.Male; }
            throw new Exception($"Invalid gender value:: {value}");
        }

        public enum ClientStatus
        {
            BLACKLISTED,
            ACTIVE,
            INACTIVE,
            PENDING_APPROVAL,
            REJECTED
        }
    }
}
