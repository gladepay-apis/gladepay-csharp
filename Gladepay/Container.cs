﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay
{
    internal class Container
    {
        private Container()
        {

        }

        private static readonly Container _instance = new Container();

        public static Container Instance => _instance;

        public Api DefaultApi { get; private set; }

        public static void SetDefaultApi(Api api)
        {
            Instance.DefaultApi = api;
        }

        public static void SetDefaultApi(string merchant_id, string merchant_key)
        {
            Instance.DefaultApi = new Api(merchant_id, merchant_key);
        }
    }
}
