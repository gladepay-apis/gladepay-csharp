﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay.Investment
{
    public class Interest
    {

        public Interest(double interestRate, InterestFrequency interestFrequency)
        {
            Rate = interestRate;
            Frequency = interestFrequency;
        }

        public double Rate { get; set; }
        public InterestFrequency Frequency { get; set; }


        public enum InterestFrequency
        {
            Annually,
            Every_Day,
            Every_Month,
            Every_Week,
            Every_Four_Weeks
        }

        public static string FrequencyToString(InterestFrequency frequency)
        {
            switch (frequency)
            {
                case InterestFrequency.Annually:
                    return "annualized";
                case InterestFrequency.Every_Day:
                    return "every_day";
                case InterestFrequency.Every_Month:
                    return "every_month";
                case InterestFrequency.Every_Four_Weeks:
                    return "every_four_weeks";
                default:
                    return null;
            }
        }

        public static InterestFrequency FrequencyFromString(string value)
        {
            if (value.Equals("every_month")) return InterestFrequency.Every_Month;
            if (value.Equals("every_week")) return InterestFrequency.Every_Week;
            if (value.Equals("every_day")) return InterestFrequency.Every_Day;
            if (value.Equals("every_four_weeks")) return InterestFrequency.Every_Four_Weeks;
            if (value.Equals("annualized")) return InterestFrequency.Annually;
            throw new Exception($"Invalid interest frequency value::: {value}");
        }



    }
}
