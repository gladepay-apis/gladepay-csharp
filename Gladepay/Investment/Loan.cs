﻿using Gladepay.Models;
using Gladepay.Payment;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay.Investment
{
    public class Loan
    {

        private static readonly Api _api = Api.GetDefaultApi();
        private static RestClient client;


        internal Loan(int id, string client_id, double amount, int period, Interest interest, bool taxApplied,
            double taxRate, string cardToken, DateTime createdAt)
        {
            Id = id;
            ClientId = client_id;
            Amount = amount;
            Period = period;
            Interest = interest;
            TaxApplied = taxApplied;
            TaxRate = taxRate;
            CardToken = cardToken;
            CreatedAt = createdAt;
        }

        public int Id { get; set; }
        public string ClientId { get; set; }
        public double Amount { get; set; }
        public int Period { get; set; }
        public Interest Interest { get; set; }
        public bool TaxApplied { get; set; }
        public double TaxRate { get; set; }
        public string CardToken { get; set; }
        public DateTime CreatedAt { get; set; }


        public static Loan Request(int client_id, double amount, Interest interest, bool taxApplied, double taxRate,
            int period, Api api = null)
        {
            var mApi = _api ?? api;
            Console.WriteLine("From loan request");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);
            var request = new RestRequest("loans", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "request",
                ["client_id"] = client_id,
                ["amount"] = amount,
                ["interestSettings"] = new JObject
                {
                    ["interestRate"] = interest.Rate,
                    ["interestFrequency"] = Interest.FrequencyToString(interest.Frequency)
                },
                ["tax_applied"] = taxApplied,
                ["tax_rate"] = taxRate,
                ["period"] = period
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            Console.WriteLine(data.ToString());
            Console.WriteLine(response.Content);
            Console.WriteLine("End loan request");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            if(code == 200)
            {
                return Parse(json["data"] as JObject);
            }

            return null;
        }


        public static Loan Get(int loan_id, Api api = null)
        {
            var mApi = _api ?? api;
            Console.WriteLine("From get loan");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);
            var request = new RestRequest("loans", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "view",
                ["loan_id"] = loan_id
               
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            Console.WriteLine(data.ToString());
            Console.WriteLine(response.Content);
            Console.WriteLine("End get loan");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            if (code == 200)
            {
                return Parse(json["data"] as JObject);
            }

            return null;
        }



        public static IList<Loan> Fetch(int offset=0,  Api api = null)
        {
            var mApi = _api ?? api;
            Console.WriteLine("From fetch loans");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);
            var request = new RestRequest("loans", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "list",
                ["offset"] = offset
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            var result = new List<Loan>();
            Console.WriteLine(data.ToString());
            Console.WriteLine(response.Content);
            Console.WriteLine("End fetch loans");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            if (code == 200)
            {
                var loans = json["data"] as JArray;
                foreach(var loan in loans)
                {
                    result.Add(Parse(loan as JObject));
                }
                return result;
            }

            return null;
        }


        public Response Process(LoanStatus status, Api api = null)
        {
            var mApi = _api ?? api;
            Console.WriteLine("From process loan");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);
            var request = new RestRequest("loans", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "process",
                ["loan_id"] = Id,
                ["status"] = status.ToString().ToLower() 
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            var result = new List<Loan>();
            Console.WriteLine(data.ToString());
            Console.WriteLine(response.Content);
            Console.WriteLine("End process loans");
            var stats = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            return new Response(stats, code, json);
        }


        public Response Repay(string ip, string fingerprint, Card card, Api api = null)
        {
            //repay via card
            var method = "card";
            var data = card.ToJson();
            return Repay(method, data as JObject, ip, fingerprint, api);
        }


        public Response Repay(double amount, string ip, string fingerprint, BankAccount account, Api api = null)
        {
            //repay via bank account
            var method = "account";
            var data = account.ToJson();
            return Repay(method, data as JObject, ip, fingerprint, api);
        }

        public Response Repay(double amount, string ip, string fingerprint, string phonenumber, string network, Api api = null)
        {
            //repay via ghana mobile money
            var method = "mobile_money";
            var data = new JObject
            {
                ["type"] = "ghana",
                ["network"] = network,
                ["phonenumber"] = phonenumber
            };
            return Repay(method, data, ip, fingerprint, api);
        }

        public Response Repay(double amount, string ip, string fingerprint, string phonenumber, Api api = null)
        {
            //repay via mpesa mobile money
            var method = "mobile_money";
            var data = new JObject
            {
                ["type"] = "mpesa",
                ["phonenumber"] = phonenumber
            };
            return Repay(method, data, ip, fingerprint, api);
        }

        public Response Repay(double amount, string ip, string fingerprint, Uri redirect_url, Api api = null)
        {
            //deposti via paga mobile money
            var method = "mobile_money";
            var data = new JObject
            {
                ["type"] = "paga_connect",
                ["redirect_url"] = redirect_url.AbsoluteUri,
            };
            return Repay(method, data, ip, fingerprint, api);
        }


        public Response Repay(double amount, string ip, string fingerprint, string accountnumber, string phonenumber, USSD.Bank bank, Api api = null)
        {
            //repay via ussd
            var method = "ussd";
            var data = new JObject
            {
                ["phonenumber"] = phonenumber,
                ["bank"] = bank.ToString().ToLower(),
                ["accountnumber"] = accountnumber
            };
            return Repay(method, data, ip, fingerprint, api);
        }

        private Response Repay(string paymentMethod, JObject paymentData, string ip, string fingerprint, Api api = null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From repay loan");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("loans", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "repay",
                ["loans_id"] = Id,
                ["payment_method"] = paymentMethod,
                ["ip"] = ip,
                ["fingerprint"] = fingerprint,
                [paymentMethod] = paymentData
            };
            Console.WriteLine(data.ToString());
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End repay");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            return new Response(status, code, json);
        }


        public static Response Validate(string txnRef, string otp, Api api = null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From validate loan");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("loans", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "validate",
                ["txnRef"] = txnRef,
                ["otp"] = otp
            };
            Console.WriteLine(data.ToString());
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End validate loans");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            return new Response(status, code, json);
        }


        public static Loan Parse(JObject loan)
        {
            var interestSettings = loan["interestSettings"] as JObject;
            var interest = new Interest((double)interestSettings["interestRate"], 
                Interest.FrequencyFromString((string)interestSettings["interestFrequency"]));
            return new Loan(
                (int)loan["id"],(string)loan["client_id"],(double)loan["amount"],(int)loan["period"],interest,
                (bool)loan["tax_applied"],(double)loan["tax_rate"],(string)loan["card_token"],DateTime.Parse((string)loan["created_at"])
                );
        }

        public enum LoanStatus
        {
            APPROVE,
            REJECT,
            CANCEL
        }
    }
}
