﻿using Gladepay.Models;
using Gladepay.Payment;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using static Gladepay.Investment.Interest;

namespace Gladepay.Investment
{
    public class Saving
    {
        private static readonly Api _api = Api.GetDefaultApi();
        private static RestClient client;

        public string Name { get; set; }
        public string ClientId { get; set; }
        public double Balance { get; set; }
        public bool IsTaxApplied { get; set; }
        public double TaxRate { get; set; }
        public string CardToken { get; set; }
        public Interest Interest { get; set; }
        public DateTime CreatedAt { get; set; }
        public int Period { get; set; }
        public int Id { get; set; }

        internal Saving(int id, string name, string client_id, double balance, bool isTaxApplied, double taxRate,
            string cardToken, Interest interest, int period,
            DateTime createdAt)
        {
            Id = id;
            Name = name;
            ClientId = client_id;
            Balance = balance;
            IsTaxApplied = isTaxApplied;
            TaxRate = taxRate;
            CardToken = cardToken;
            Interest = interest;
            CreatedAt = createdAt;
            Period = period;
        }

        public static Saving Create(string name, int client_id,int period,
            Interest interest, bool isTaxApplied, double taxRate, Api api=null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From create savings");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("investments", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "create_savings",
                ["name"] = name,
                ["client_id"] = client_id,
                ["tax_applied"] = isTaxApplied,
                ["tax_rate"] = taxRate,
                ["period"] = period,
                ["interestSettings"] = new JObject
                {
                    ["interestRate"] = interest.Rate,
                    ["interestFrequency"] = Interest.FrequencyToString(interest.Frequency)
                }
            };
            Console.WriteLine(data.ToString());
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End create savings");
            var stats = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            if (code == 200)
            {
                var saving_data = json["data"] as JObject;
                return Parse(saving_data);
            }
            return null;
        }



        public static IList<Saving> Fetch(int client_id=0, int offset=0, Api api = null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From list savings");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("investments", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "list_savings",
                ["offset"] = offset
            };
            if(client_id > 0) { data["client_id"] = client_id; }
            Console.WriteLine(data.ToString());
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            var result = new List<Saving>();

            Console.WriteLine(response.Content);
            Console.WriteLine("End list savings");
            var stats = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            if (code == 200)
            {
                var savings_data = json["data"] as JArray;
                foreach(var i in savings_data)
                {
                    result.Add(Parse(i as JObject));
                }
                return result;
            }
            return null;
        }


        public static Saving Get(int id, Api api = null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From get saving");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("investments", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "view_savings",
                ["savings_id"] = id
            };
            Console.WriteLine(data.ToString());
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End get saving");
            var stats = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            if (code == 200)
            {
                var saving_data = json["data"] as JObject;
                return Parse(saving_data);
            }
            return null;
        }


        public Response Cashout(int client_id, double amount, Cashout method, Api api=null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From cashout saving");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("investments", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "cashout",
                ["saving_id"] = Id,
                ["client_id"] = client_id,
                ["amount"] = amount,
                ["method"] = method.ToString().ToLower()
            };
            Console.WriteLine(data.ToString());
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End cashout saving");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            return new Response(status, code, json);
        }

        public Response Deposit(double amount, string ip, string fingerprint, Card card, Api api = null)
        {
            //deposit via card
            var method = "card";
            var data = card.ToJson();
            return Deposit(method, data as JObject, amount, ip, fingerprint, api);
        }

        public Response Deposit(double amount, string ip, string fingerprint, BankAccount account, Api api = null)
        {
            //deposit via bank account
            var method = "account";
            var data = account.ToJson();
            return Deposit(method, data as JObject, amount, ip, fingerprint, api);
        }

        public Response Deposit(double amount, string ip, string fingerprint, string phonenumber, string network, Api api = null)
        {
            //deposit via ghana mobile money
            var method = "mobile_money";
            var data = new JObject
            {
                ["type"] = "ghana",
                ["network"] = network,
                ["phonenumber"] = phonenumber
            };
            return Deposit(method, data, amount, ip, fingerprint, api);
        }

        public Response Deposit(double amount, string ip, string fingerprint, string phonenumber, Api api = null)
        {
            //deposit via mpesa mobile money
            var method = "mobile_money";
            var data = new JObject
            {
                ["type"] = "mpesa",
                ["phonenumber"] = phonenumber
            };
            return Deposit(method, data, amount, ip, fingerprint, api);
        }

        public Response Deposit(double amount, string ip, string fingerprint, Uri redirect_url, Api api = null)
        {
            //deposti via paga mobile money
            var method = "mobile_money";
            var data = new JObject
            {
                ["type"] = "paga_connect",
                ["redirect_url"] = redirect_url.AbsoluteUri,
            };
            return Deposit(method, data, amount, ip, fingerprint, api);
        }


        public Response Deposit(double amount, string ip, string fingerprint, string accountnumber, string phonenumber, USSD.Bank bank, Api api = null)
        {
            //deposit via ussd
            var method = "ussd";
            var data = new JObject
            {
                ["phonenumber"] = phonenumber,
                ["bank"] = bank.ToString().ToLower(),
                ["accountnumber"] = accountnumber
            };
            return Deposit(method, data, amount, ip, fingerprint, api);
        }

        private Response Deposit(string paymentMethod, JObject paymentData, double amount, string ip, string fingerprint, Api api=null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From deposit saving");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("investments", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "deposit",
                ["savings_id"] = Id,
                ["payment_method"] = paymentMethod,
                ["amount"] = amount,
                ["ip"] = ip,
                ["fingerprint"] = fingerprint,
                [paymentMethod] = paymentData
            };
            Console.WriteLine(data.ToString());
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End deposit");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            return new Response(status, code, json);
        }


        public static Response Validate(string txnRef, string otp, Api api = null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From validate saving");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("investments", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "validate",
                ["txnRef"] = txnRef,
                ["otp"] = otp
            };
            Console.WriteLine(data.ToString());
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End validate savings");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            return new Response(status, code, json);
        }


        public static Saving Parse(JObject saving)
        {
            var interestSettings = saving["interestSettings"] as JObject;
            var interest = new Interest((double)interestSettings["interestRate"],
                Interest.FrequencyFromString((string)interestSettings["interestFrequency"]));
            return new Saving((int)saving["id"], (string)saving["name"], (string)saving["client_id"],
                (double)saving["balance"],(bool)saving["tax_applied"],(double)saving["tax_rate"],
                (string)saving["card_token"],interest,
                (int)saving["period"],DateTime.Parse((string)saving["created_at"]));

        }

       
    }

    

    public enum Cashout
    {
        Cash,
        Transfer,
        Cashcode
    }
}
