﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay.Investment.Thrift
{
    public class Plan
    {
        private static readonly Api _api = Api.GetDefaultApi();
        private static RestClient client;

        public Plan(string name, double amount, DateTime startDate, int required_number_of_members,
            PlanFrequency frequency)
        {
            Name = name;
            Amount = amount;
            StartDate = startDate;
            RequiredNumberOfMembers = required_number_of_members;
            Frequency = frequency;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public double Amount { get; set; }
        public DateTime StartDate { get; set; }
        public int RequiredNumberOfMembers { get; set; }
        public PlanFrequency Frequency { get; set; }
        public DateTime CreatedAt { get; set; }
        public PlanStatus Status { get; set; }


        public Response Subscribe(int savings_id, Api api = null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From subscribe to thrift plan");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("investments", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "subscribe",
                ["savings_id"] = savings_id,
                ["thrift_id"] = Id,

            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End subscribe to  thrift plan");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            return new Response(status, code, json);
        }


        public Response Create(Api api = null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From create thrift plan");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("investments", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "create_plan",
                ["name"] = Name,
                ["amount"] = Amount,
                ["start_date"] = StartDate.ToString("dd-MM-yyyy"),
                ["member_required"] = RequiredNumberOfMembers,
                ["frequency"] = FrequencyToString(Frequency)

            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End create thrift plan");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var plan_data = json["data"] as JObject;
            var plan_id = (int)plan_data["id"];
            var plan_created_date = DateTime.Parse((string)plan_data["created_at"]);
            Id = plan_id;
            CreatedAt = plan_created_date;
            var code = (int)json["status"];
            return new Response(status, code, json);
        }

        public IList<Subscription> FetchSubscriptions(Api api = null)
        {
            return Subscription.Fetch(Id, api);
        }

        public Response Payout(Api api = null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From pyaout thrift plan");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("investments", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "payout",
                ["thrift_id"] = Id

            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End payout thrift plan");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            return new Response(status, code, json);
        }


        public static IList<Plan> Fetch(int offset = 0, Api api = null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From list thrift plans");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("investments", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "list_plans",
                ["offset"] = offset
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            var result = new List<Plan>();
            if (response.IsSuccessful)
            {
                Console.WriteLine(response.Content);
                Console.WriteLine("End create thrift plan");
                var status = response.StatusCode;
                var json = JToken.Parse(response.Content);
                var plans_array = json["data"] as JArray;
                foreach (var plan_data in plans_array)
                {
                    var plan = plan_data as JObject;
                    result.Add(Parse(plan));
                }
                return result;
            }
            else
            {
                throw new Exception("Unbale to fetch thrift plans");
            }

        }


        private static Plan Parse(JObject plan)
        {
            var name = (string)plan["name"];
            var id = (int)plan["id"];
            var status = PlanStatusFromString((string)plan["status"]);
            var amount = (double)plan["amount"];
            var start_date = DateTime.Parse((string)plan["start_date"]);
            var member_required = (int)plan["member_required"];
            var created_at = DateTime.Parse((string)plan["created_at"]);
            var frequency = FrequencyFromString((string)plan["frequency"]);
            return new Plan(name, amount, start_date, member_required, frequency)
            {
                Id = id,
                Status = status,
                CreatedAt = created_at
            };
        }

        public enum PlanFrequency
        {
            Annually,
            Every_Month,
            Every_Week,
            Every_Day,
            Every_Four_Weeks
        }

        public enum PlanStatus
        {
            PENDING,
            STARTED,
            COMPLETED
        }

        public static string FrequencyToString(PlanFrequency frequency)
        {
            switch (frequency)
            {
                case PlanFrequency.Annually:
                    return "annualized";
                case PlanFrequency.Every_Day:
                    return "every_day";
                case PlanFrequency.Every_Month:
                    return "every_month";
                case PlanFrequency.Every_Four_Weeks:
                    return "every_four_weeks";
                default:
                    return null;
            }
        }

        public static PlanFrequency FrequencyFromString(string value)
        {
            if (value.Equals("every_month")) return PlanFrequency.Every_Month;
            if (value.Equals("every_week")) return PlanFrequency.Every_Week;
            if (value.Equals("every_day")) return PlanFrequency.Every_Day;
            if (value.Equals("every_four_weeks")) return PlanFrequency.Every_Four_Weeks;
            if (value.Equals("annualized")) return PlanFrequency.Annually;
            throw new Exception("Invalid thrift frequency value");
        }

        public static PlanStatus PlanStatusFromString(string value)
        {
            if (value.Equals("pending")) return PlanStatus.PENDING;
            if (value.Equals("started")) return PlanStatus.STARTED;
            if (value.Equals("completed")) return PlanStatus.COMPLETED;
            throw new Exception("Invalid plan status value");
        }

    }
}
