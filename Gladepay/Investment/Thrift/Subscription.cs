﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay.Investment.Thrift
{
    public class Subscription
    {
        private static readonly Api _api = Api.GetDefaultApi();
        private static RestClient client;

        internal Subscription(int id, int plan_id, int savings_id, int position, SubscriptionStatus status,
            DateTime createdAt)
        {
            Id = id;
            PlanId = plan_id;
            SavingId = savings_id;
            Position = position;
            Status = status;
            CreatedAt = createdAt;
        }

        public int Id { get; set; }
        public int PlanId { get; set; }
        public int SavingId { get; set; }
        public int Position { get; set; }
        public DateTime CreatedAt { get; set; }
        public SubscriptionStatus Status { get; set; }


        public static IList<Subscription> Fetch(int plan_id, Api api = null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From fetch plan subscriptions");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("investments", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "subscriptions",
                ["thrift_id"] = plan_id
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            var result = new List<Subscription>();
            if (response.IsSuccessful)
            {
                Console.WriteLine(response.Content);
                Console.WriteLine("End list plan subscriptions");
                var status = response.StatusCode;
                var json = JToken.Parse(response.Content);
                var sub_array = json["subscriptions"] as JArray;
                foreach (var sub_data in sub_array)
                {
                    var sub = sub_data as JObject;
                    result.Add(Parse(sub));
                }
                return result;
            }
            else
            {
                throw new Exception("Unbale to fetch subscriptions");
            }

        }


        private static Subscription Parse(JObject sub)
        {
            var id = (int)sub["id"];
            var status = StatusFromString((string)sub["status"]);
            var position = (int)sub["position"];
            var plan_id = (int)sub["thrift_id"];
            var created_at = DateTime.Parse((string)sub["created_at"]);
            var savings_id = (int)sub["savings_id"];
            return new Subscription(id, plan_id, savings_id, position, status, created_at);
            
        }

        public enum SubscriptionStatus
        {
            PENDING,
            PAID
        }

        public static SubscriptionStatus StatusFromString(string value)
        {
            if (value.Equals("pending")) return SubscriptionStatus.PENDING;
            if (value.Equals("paid")) return SubscriptionStatus.PAID;
            throw new Exception("Invalid plan status value");
        }

    }
}
