﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay.Models
{
    public class BankAccount
    {
        public BankAccount(string accountNumber, string bankCode)
        {
            AccountNumber = accountNumber;
            BankCode = bankCode;
        }

        public BankAccount(string accountNumber, Bank bank):this(accountNumber, bank.Code)
        {

        }

        public string AccountNumber { get; set; }
        public string BankCode { get; set; }

        public static BankAccount Parse(JObject bank)
        {
            return new BankAccount(
                (string)bank["accountnumber"],
                (string)bank["bankcode"]
                );
        }

        public JToken ToJson()
        {
            return new JObject
            {
                ["accountnumber"] = AccountNumber,
                ["bankcode"] = BankCode
            };
        }
    }
}
