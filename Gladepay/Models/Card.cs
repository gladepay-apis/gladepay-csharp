﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay.Models
{
    public class Card
    {
        public Card(string card_number, int expiry_month, int expiry_year, string ccv, string pin)
        {
            CardNumber = card_number;
            ExpiryMonth = expiry_month;
            ExpiryYear = expiry_year;
            Ccv = ccv;
            Pin = pin;
        }

        public string CardNumber { get; set; }
        public int ExpiryMonth { get; set; }
        public int ExpiryYear { get; set; }
        public string Ccv { get; set; }
        public string Pin { get; set; }

        public JToken ToJson()
        {
            return new JObject
            {
                ["card_no"] = CardNumber,
                ["expiry_month"] = ExpiryMonth,
                ["expiry_year"] = ExpiryYear,
                ["ccv"] = Ccv,
                ["pin"] = Pin,
            };
        }
    }
}
