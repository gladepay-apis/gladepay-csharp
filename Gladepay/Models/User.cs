﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay.Models
{
    public class User
    {
        public User(string firstname, string lastname, string email, string ip, string fingerprint)
        {
            FirstName = firstname;
            LastName = lastname;
            Email = email;
            Ip = ip;
            FingerPrint = fingerprint;
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Ip { get; set; }
        public string FingerPrint { get; set; }


        public JToken ToJson()
        {
            return new JObject
            {
                ["firstname"] = FirstName,
                ["lastname"] = LastName,
                ["email"] = Email,
                ["ip"] = Ip,
                ["fingerprint"] = FingerPrint,
            };
        }

    }
}
