﻿using Gladepay.Models;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay.Payment
{
    public class AccountPayment
    {

        private static readonly Api _api = Api.GetDefaultApi();
        private static RestClient client;

        public static Response Charge(User user, BankAccount account, double amount,
            Installment installment = null, IList<Split> split = null, Api api = null)
        {
            var mApi = _api ?? api;
            Console.WriteLine("From Charge Accountpayment");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);
            var request = new RestRequest("payment", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "charge",
                ["paymentType"] = "account",
                ["amount"] = amount,
                ["user"] = user.ToJson(),
                ["account"] = account.ToJson()
            };
            if (installment != null)
            {
                //initiate installmental payment
                installment.Validate(amount);
                data["installment"] = installment.ToJson();
                Console.WriteLine("Complete installment payload");
                Console.WriteLine(data.ToString());
                Console.WriteLine("end installment payload");

            }
            if (split != null)
            {
                var result = new JArray();
                foreach (var s in split)
                {
                    s.Validate(amount);
                    result.Add(s.ToJson());
                }
                data["split"] = result;
                Console.WriteLine("Complete split payload");
                Console.WriteLine(data.ToString());
                Console.WriteLine("end split payload");
            }
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            Console.WriteLine(data.ToString());
            Console.WriteLine(response.Content);
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            Console.WriteLine("End account charge");
            return new Response(status, code, json);
        }

        public static Response Validate(string txnRef, string otp, Api api = null)
        {

            var mApi = api ?? _api;
            Console.WriteLine("From Validate card charage payment");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("payment", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "validate",
                ["txnRef"] = txnRef,
                ["validate"] = "account",
                ["otp"] = otp
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End validation card payment");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            return new Response(status, code, json);
        }
    }
}
