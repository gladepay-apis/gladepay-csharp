﻿using Gladepay.Models;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Gladepay.Payment
{
    public class CardPayment
    {
        private static readonly Api _api = Api.GetDefaultApi();
        private static RestClient client;

        public enum RecurringFrequency
        {
            DAILY,
            WEEKLY,
            MONTHLY
        }

        public class Recurrent
        {
            public Recurrent(RecurringFrequency frequency, int value)
            {
                Frequency = frequency;
                Value = value;
            }

            public RecurringFrequency Frequency { get; set; }
            public int Value { get; set; }


            public void Validate()
            {
                if(Frequency == RecurringFrequency.DAILY && (Value < 0 || Value > 23))
                {
                    throw new Exception("Invalid recurring value, for daily recurrent payment pass value between 0 and 23");
                }
                else if(Frequency == RecurringFrequency.WEEKLY && (Value < 1 || Value > 7))
                {
                    throw new Exception("Invalid recurring value, for weekly recurrent payment pass value between 1 and 7");
                }
                else if (Frequency == RecurringFrequency.MONTHLY && (Value < 1 || Value > 30))
                {
                    throw new Exception("Invalid recurring value, for monthly recurrent payment pass value between 1 and 30");
                }
            }

            public JToken ToJson()
            {
                var result = new JObject
                {
                    ["frequency"] = Frequency.ToString().ToLower(),
                    ["value"] = Value
                };
                Console.WriteLine("From Recurrent to json");
                Console.WriteLine(result.ToString());
                Console.WriteLine("End recurrent to json");
                return result;
            }
        }

        public static Response Initiate(User user, Card card, double amount, string country="NG", 
            string currency="NGN", Installment installment=null, Recurrent recurrent=null, 
            IList<Split> split=null, Api api=null)
        {
            var mApi = _api ?? api;
            Console.WriteLine("From Initiate Card Payment");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);
            var request = new RestRequest("payment", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "initiate",
                ["paymentType"] = "card",
                ["amount"] = amount,
                ["country"] = country,
                ["currency"] = currency,
                ["user"] = user.ToJson(),
                ["card"] = card.ToJson()
            };
            if(installment != null && recurrent != null)
            {
                throw new Exception("Cannot Initiate a recurring and installmental payment");
            }
            if(installment != null)
            {
                //initiate installmental payment
                installment.Validate(amount);
                data["installment"] = installment.ToJson();
                Console.WriteLine("Complete installment payload");
                Console.WriteLine(data.ToString());
                Console.WriteLine("end installment payload");

            }
            if(recurrent != null)
            {
                recurrent.Validate();
                data["recurrent"] = recurrent.ToJson();
                Console.WriteLine("Complete recurrent payload");
                Console.WriteLine(data.ToString());
                Console.WriteLine("end recurrent payload");
            }
            if(split != null)
            {
                var result = new JArray();
                foreach(var s in split)
                {
                    s.Validate(amount);
                    result.Add(s.ToJson());
                }
                data["split"] = result;
                Console.WriteLine("Complete split payload");
                Console.WriteLine(data.ToString());
                Console.WriteLine("end split payload");
            }
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            Console.WriteLine(response.Content);
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            Console.WriteLine("End Initiate Cardpayment");
            return new Response(status,code, json);

        }


        public static Response Charge(User user, Card card, double amount, string txnRef,
            string auth_type, string country = "NG", string currency = "NGN", Api api = null)
        {
            var mApi = _api ?? api;
            Console.WriteLine("From Charge Cardpayment");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);
            var request = new RestRequest("payment", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "charge",
                ["paymentType"] = "card",
                ["amount"] = amount,
                ["country"] = country,
                ["currency"] = currency,
                ["user"] = user.ToJson(),
                ["card"] = card.ToJson(),
                ["txnRef"] = txnRef,
                ["auth_type"] = auth_type
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            Console.WriteLine(data.ToString());
            Console.WriteLine(response.Content);
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            Console.WriteLine("End card charge");
            return new Response(status, code, json);
        }


        public static Response TokenCharge(User user, double amount, string token,
            Recurrent recurrent = null, Api api = null)
        {
            var mApi = _api ?? api;
            Console.WriteLine("From Token Charge Payment");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);
            var request = new RestRequest("payment", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "charge",
                ["paymentType"] = "token",
                ["amount"] = amount,
                ["token"] = token,
                ["user"] = user.ToJson()
            };
            if (recurrent != null)
            {
                recurrent.Validate();
                data["recurrent"] = recurrent.ToJson();
                Console.WriteLine("Complete recurrent payload");
                Console.WriteLine(data.ToString());
                Console.WriteLine("end recurrent payload");
            }
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];

            Console.WriteLine(response.Content);
            Console.WriteLine("End of Token charge");
            return new Response(status, code, json);
        }

        public static Response Validate(string txnRef, string otp, Api api = null)
        {

            var mApi = api ?? _api;
            Console.WriteLine("From Validate card charage payment");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("payment", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "validate",
                ["txnRef"] = txnRef,
                ["otp"] = otp
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            
            Console.WriteLine(response.Content);
            Console.WriteLine("End validation card payment");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            return new Response(status, code, json);
        }


        public static Response Verify(string txnRef, Api api = null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From Verify card charage payment");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("payment", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "verify",
                ["txnRef"] = txnRef
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End verify card payment");
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            return new Response(status, code, json);
        }


        public static double GetTransactionFee(string cardNumber, double amount, Api api = null)
        {
            var mApi = api ?? _api;
            Console.WriteLine("From get transaction fee");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);

            var request = new RestRequest("resources", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["inquire"] = "charges",
                ["card_no"] = cardNumber,
                ["amount"] = amount
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);

            Console.WriteLine(response.Content);
            Console.WriteLine("End  get transaction fee");
            if (response.IsSuccessful)
            {
                return double.Parse(response.Content);
            }
            return 0;
        }

    }
}
