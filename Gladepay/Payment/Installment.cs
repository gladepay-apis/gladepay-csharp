﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay.Payment
{
    public class Installment
    {
        public Installment(IList<PaymentSchedule> schedules, double totalAmount)
        {
            Schedules = schedules;
            TotalAmount = totalAmount;
        }

        public IList<PaymentSchedule> Schedules { get; set; }
        public double TotalAmount { get; set; }

        public void Validate(double initial_payment)
        {
            var ipp = ((initial_payment * 100) / TotalAmount); //initial payment percentage
            var remainingPercentage = 100 - ipp;
            var totalSchedulePercentage = 0;
            foreach (var i in Schedules)
            {
                if (i.Date <= DateTime.Now)
                {
                    throw new Exception("Invalid installmental schedule dates");
                }
                totalSchedulePercentage += i.Percentage;
            }
            if (totalSchedulePercentage != remainingPercentage)
            {
                throw new Exception("Invalid installmental schedule ratios");
            }
        }

        public JObject ToJson()
        {
            var schedule_object = new JObject();
            foreach (var i in Schedules)
            {
                schedule_object[i.Date.ToString("dd-MM-yyyy")] = i.Percentage;
            }
            var result = new JObject
            {
                ["payment_schedule"] = schedule_object,
                ["total"] = TotalAmount
            };
            Console.WriteLine("From payment schedule to json");
            Console.WriteLine(result.ToString());
            Console.WriteLine("End payment schedule to json");
            return result;
        }
    }
}
