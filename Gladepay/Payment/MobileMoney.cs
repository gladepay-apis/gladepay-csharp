﻿using Gladepay.Models;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay.Payment
{
    public class MobileMoney
    {
        private static readonly Api _api = Api.GetDefaultApi();
        private static RestClient client;

        public static Response GhanaCharge(User user, string phoneNumber, string Network, double amount, 
            Api api = null)
        {
            var mApi = _api ?? api;
            Console.WriteLine("From Ghana mobile money");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);
            var request = new RestRequest("payment", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "charge",
                ["paymentType"] = "mobile_money",
                ["type"] = "ghana",
                ["amount"] = amount,
                ["phonenumber"] = phoneNumber,
                ["network"] = Network,
                ["user"] = user.ToJson()
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            Console.WriteLine(data.ToString());
            Console.WriteLine(response.Content);
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            Console.WriteLine("End Ghana mobile money");
            return new Response(status, code, json);
        }


        public static Response MpesaCharge(User user, string phoneNumber, double amount, Api api = null)
        {
            var mApi = _api ?? api;
            Console.WriteLine("From Mpesa mobile money");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);
            var request = new RestRequest("payment", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "charge",
                ["paymentType"] = "mobile_money",
                ["type"] = "mpesa",
                ["amount"] = amount,
                ["phonenumber"] = phoneNumber,
                ["user"] = user.ToJson()
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            Console.WriteLine(data.ToString());
            Console.WriteLine(response.Content);
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            Console.WriteLine("End Ghana mobile money");
            return new Response(status, code, json);
        }


        public static Response PagaCharge(User user, string redirect_url, double amount,Api api = null)
        {
            var mApi = _api ?? api;
            Console.WriteLine("From Paga mobile money");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);
            var request = new RestRequest("payment", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "charge",
                ["paymentType"] = "mobile_money",
                ["type"] = "paga_connect",
                ["amount"] = amount,
                ["redirect_url"] = redirect_url,
                ["user"] = user.ToJson()
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            Console.WriteLine(data.ToString());
            Console.WriteLine(response.Content);
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            Console.WriteLine("End Paga mobile money");
            return new Response(status, code, json);
        }
    }
}
