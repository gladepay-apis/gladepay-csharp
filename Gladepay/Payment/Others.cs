﻿using Gladepay.Models;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay.Payment
{
    public class QRPay
    {
        private static readonly Api _api = Api.GetDefaultApi();
        private static RestClient client;

        public static Response Charge(User user, string businessName, double amount, Api api = null)
        {
            var mApi = _api ?? api;
            Console.WriteLine("From Qrpay money");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);
            var request = new RestRequest("payment", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "charge",
                ["paymentType"] = "qrpay",
                ["amount"] = amount,
                ["business_name"] = businessName,
                ["user"] = user.ToJson()
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            Console.WriteLine(data.ToString());
            Console.WriteLine(response.Content);
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            Console.WriteLine("End qrpay");
            return new Response(status, code, json);
        }
    }

    public class USSD
    {
        public enum Bank
        {
            ZENITH,
            GTB
        }

        private static readonly Api _api = Api.GetDefaultApi();
        private static RestClient client;

        public static Response Charge(User user, string phoneNumber, Bank bank, string accountNumber,
            double amount, Api api = null)
        {
            var mApi = _api ?? api;
            Console.WriteLine("From ussd pay");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);
            var request = new RestRequest("payment", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "charge",
                ["paymentType"] = "ussd",
                ["amount"] = amount,
                ["phonenumber"] = phoneNumber,
                ["bank"] = bank.ToString().ToLower(),
                ["accountnumber"]=accountNumber,
                ["user"] = user.ToJson()
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            Console.WriteLine(data.ToString());
            Console.WriteLine(response.Content);
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            Console.WriteLine("End ussd pay");
            return new Response(status, code, json);
        }
    }

    public class PhonePay
    {
        private static readonly Api _api = Api.GetDefaultApi();
        private static RestClient client;

        public static Response Charge(User user, string phoneNumber, double amount,
            string country="NG", string currency="NGN", Api api = null)
        {
            var mApi = _api ?? api;
            Console.WriteLine("From Ghana mobile money");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);
            var request = new RestRequest("payment", Method.PUT, DataFormat.Json);
            var data = new JObject
            {
                ["action"] = "charge",
                ["paymentType"] = "phone",
                ["amount"] = amount,
                ["phonenumber"] = phoneNumber,
                ["country"] = country,
                ["currency"] = currency,
                ["user"] = user.ToJson()
            };
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            Console.WriteLine(data.ToString());
            Console.WriteLine(response.Content);
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];
            Console.WriteLine("End phone pay");
            return new Response(status, code, json);
        }
    }
}
