﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay.Payment
{
    public class PaymentSchedule
    {
        public PaymentSchedule(DateTime date, int percentage)
        {
            Date = date;
            Percentage = percentage;

        }

        public DateTime Date { get; set; }
        public int Percentage { get; set; }
    }
}
