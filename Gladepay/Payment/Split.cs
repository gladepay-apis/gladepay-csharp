﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay.Payment
{
    public class Split
    {
        public Split(string refCode, int percentage)
        {
            RefCode = refCode;
            Percentage = percentage;
        }

        public Split(string refCode, double fixedShare)
        {
            RefCode = refCode;
            FixedShare = fixedShare;
        }

        public string RefCode { get; set; }
        public int Percentage { get; set; }
        public double FixedShare { get; set; }

        public void Validate(double amount)
        {
            if(FixedShare < 0)
            {
                throw new Exception("Fixedshare value must be greater than zero");
            }
            if(FixedShare > amount)
            {
                throw new Exception("Fixedshare value must not be greater than the total amount");
            }
            if(Percentage < 0)
            {
                throw new Exception("Percentage value must be greater than zero");
            }
            if(Percentage > 100)
            {
                throw new Exception("Percentage can not be greater than 100");
            }
        }

        public JToken ToJson()
        {
            var result = new JObject { ["ref_code"] = RefCode };
            if (FixedShare > 0) { result["share"] = FixedShare; }
            else if(Percentage > 0){ result["percentage"] = Percentage; }
            Console.WriteLine("From split to json");
            Console.WriteLine(result.ToString());
            Console.WriteLine("End split to json");
            return result;
        }
    }
}
