﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Gladepay
{
    public class Response
    {
        public Response(HttpStatusCode status, int code, JToken json)
        {
            Status = status;
            StatusCode = code;
            Json = json;
        }

        public Response(HttpStatusCode status, string statusMessage, JToken json)
        {
            Status = status;
            StatusMessage = statusMessage;
            Json = json;
        }

        public HttpStatusCode Status { get; set; }
        public int StatusCode { get; set; }
        public string StatusMessage { get; set; }
        public JToken Json { get; set; }
    }
}
