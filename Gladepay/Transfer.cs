﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace Gladepay
{
    public class Transfer
    {
        private static readonly Api _api = Api.GetDefaultApi();
        private static RestClient client;

        public class TransferData
        {
            public TransferData(double amount, string bankCode, string accountNumber,
            string senderName, string orderRef, string narration = "")
            {
                Amount = amount;
                BankCode = bankCode;
                AccountNumber = accountNumber;
                SenderName = senderName;
                OrderRef = orderRef;
                Narration = narration;
            }

            public double Amount { get; set; }
            public string BankCode { get; set; }
            public string AccountNumber { get; set; }
            public string SenderName { get; set; }
            public string OrderRef { get; set; }
            public string Narration { get; set; }


            public JToken ToJson()
            {
                var result = new JObject
                {
                    ["bankcode"] = BankCode,
                    ["amount"] = Amount,
                    ["accountnumber"] = AccountNumber,
                    ["sender_name"] = SenderName,
                    ["narration"] = Narration,
                    ["orderRef"] = OrderRef
                };
                Console.WriteLine("From Transfer Data");
                Console.WriteLine(result.ToString());
                Console.WriteLine("End Transfer Data");
                return result;
            }
        }

        public static Response SingleTransfer(TransferData transferData, Api api=null)
        {
            var mApi = _api ?? api;
            var data = transferData.ToJson();
            data["action"] = "transfer";
            Console.WriteLine("From Single Transfer Disbursement");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            Console.WriteLine($"Complete single transfer payload {data.ToString()}");
            client = new RestClient(mApi.RootUrl);
            var request = new RestRequest("disburse", Method.PUT, DataFormat.Json);
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];

            Console.WriteLine(response.Content);
            Console.WriteLine("End of single transfer");
            return new Response(status, code, json);
        }

        public static Response BulkTransfer(IList<TransferData> transferDatas, Api api=null)
        {
            var mApi = _api ?? api;
            Console.WriteLine("From Bulk Transfer Disbursement");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            client = new RestClient(mApi.RootUrl);
            var request = new RestRequest("disburse", Method.PUT, DataFormat.Json);
            var data = new JArray();
            foreach(var t in transferDatas)
            {
                data.Add(t.ToJson());
            }
            var payload = new JObject
            {
                ["action"] = "transfer",
                ["type"] = "bulk",
                ["data"] = data
            };
            Console.WriteLine($"Complete single transfer payload {payload.ToString()}");
            request.AddJsonBody(payload.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];

            Console.WriteLine(response.Content);
            Console.WriteLine("End of bulk transfer");
            return new Response(status, code, json);
        }

        public static Response VerifyTransfer(string txnRef, Api api=null)
        {
            var mApi = _api ?? api;
            var data = new JObject
            {
                ["action"] = "verify",
                ["txnRef"] = txnRef
            };
            Console.WriteLine("From Verify Transfer ");
            Console.WriteLine($"Merchant ID: {mApi.MerchantId}");
            Console.WriteLine($"Merchant Key: {mApi.MerchantKey}");
            Console.WriteLine($"Complete verify transfer payload {data.ToString()}");
            client = new RestClient(mApi.RootUrl);
            var request = new RestRequest("disburse", Method.PUT, DataFormat.Json);
            request.AddJsonBody(data.ToString());

            //add headers
            request.AddHeader("key", mApi.MerchantKey);
            request.AddHeader("mid", mApi.MerchantId);

            var response = client.Execute(request);
            var status = response.StatusCode;
            var json = JToken.Parse(response.Content);
            var code = (int)json["status"];

            Console.WriteLine(response.Content);
            Console.WriteLine("End of verify transfer");
            return new Response(status, code, json);
        }
    }
}
