﻿using System;
using System.Collections.Generic;
using System.Net;
using Gladepay.Bill;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    [TestClass]
    public class BillTest
    {
        [TestMethod]
        public void TestFetchBills()
        {
            var bills = Bill.Fetch();
            Assert.IsInstanceOfType(bills, typeof(IList<Bill>));
            Assert.IsTrue(bills.Count > 0);
        }

        [TestMethod]
        public void TestPayBill()
        {
            var bills = Bill.Fetch();
            var bill = bills[0];
            var ct = getCurrentTime();
            var response = bill.Pay(30000, "1234567890", $"gpcl-{ct.ToString()}-bp");
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(200, response.StatusCode);
            Assert.IsNotNull((string)response.Json["txnRef"]);
        }

        [TestMethod]
        public void TestResolveBillForNonRequired()
        {
            var bills = Bill.Fetch();
            var bill = bills[0];
            Assert.IsFalse(bill.RequireNameQuery);
            Assert.ThrowsException<Exception>(() =>
            {
                var response = bill.ResolveName(10000, "0000000001");
            });
        }

        [TestMethod]
        public void TestResolveBillForRequired()
        {
            var bills = Bill.Fetch();
            var bill = bills[bills.Count-1];
            Assert.IsTrue(bill.RequireNameQuery);
            var response = bill.ResolveName(10000, "0000000001");
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(200, response.StatusCode);
        }

        [TestMethod]
        public void TestVerifyBillPayment()
        {
            var txnRef = "GP|BP|53152590020190410N";
            var response = Bill.VerifyPayment(txnRef);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(txnRef, (string)response.Json["txnRef"]);
            Assert.AreEqual("opww", (string)response.Json["orderRef"]);
        }

        public long getCurrentTime()
        {
            return (long)DateTime.Now.Subtract(DateTime.MinValue.AddYears(1969)).TotalMilliseconds;

        }
    }
}
