﻿using System;
using System.Collections.Generic;
using System.Net;
using Gladepay;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    [TestClass]
    public class GeneralTest
    {
        [TestMethod]
        public void TestCreateClient()
        {
            var ct = getCurrentTime();
            var bp = ct.ToString().Substring(0, 11);
            var client = Client.Create("Jane", "Doe", $"janedoe{ct.ToString()}@gmail.com",
                bp, bp);
            Assert.IsNotNull(client);
            Assert.IsInstanceOfType(client, typeof(Client));
            Assert.IsTrue(client.Id > 0);
        }


        [TestMethod]
        public void TestFetchClient()
        {
            var clients = Client.Fetch();
            Assert.IsNotNull(clients);
            Assert.IsInstanceOfType(clients, typeof(IList<Client>));
            Assert.IsTrue(clients.Count > 0);
        }


        [TestMethod]
        public void TestUpdateClient()
        {
            var client = Client.Update(3, firstname: "Update", lastname: "Client", email: "fromc#libarystatictests@gmail.com");
            Assert.IsNotNull(client);
            Assert.IsInstanceOfType(client, typeof(Client));
            Assert.IsTrue(client.Id == 3);
        }

        [TestMethod]
        public void TestGetClient()
        {
            var client = Client.Get(3);
            Assert.IsNotNull(client);
            Assert.IsInstanceOfType(client, typeof(Client));
            Assert.IsTrue(client.Id == 3);
        }


        public long getCurrentTime()
        {
            return (long)DateTime.Now.Subtract(DateTime.MinValue.AddYears(1969)).TotalMilliseconds;

        }
    }
}
