﻿using System;
using System.Collections.Generic;
using System.Net;
using Gladepay.Investment;
using Gladepay.Investment.Thrift;
using Gladepay.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    [TestClass]
    public class InvestmentTest
    {
        [TestMethod]
        public void TestCreateThriftPlan()
        {
            var plan = new Plan("Gladepay c# lib plan", 23500.50, DateTime.Now, 5,
                 Plan.PlanFrequency.Every_Day);
            Assert.AreEqual(0,plan.Id);
            var response = plan.Create();
            Assert.IsNotNull(plan.Id);
            Assert.IsTrue(plan.Id > 0);
            Assert.IsNotNull(plan.CreatedAt);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(200, response.StatusCode);
        }

        [TestMethod]
        public void TestListThriftPlans()
        {
            var plans = Plan.Fetch();
            Assert.IsInstanceOfType(plans, typeof(IList<Plan>));
            Assert.IsTrue(plans.Count > 0);
            
        }



        [TestMethod]
        public void TestSubscribeToThriftPlans()
        {
            var plans = Plan.Fetch();
            Assert.IsInstanceOfType(plans, typeof(IList<Plan>));
            Assert.IsTrue(plans.Count > 0);
            var plan = plans[0];
            var response = plan.Subscribe(7);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(104, response.StatusCode);
        }

        [TestMethod]
        public void TestFetchSubscriptionsWithPlanId()
        {
            var subscriptions = Subscription.Fetch(2);
            Assert.IsInstanceOfType(subscriptions, typeof(IList<Subscription>));
            Assert.IsTrue(subscriptions.Count > 0);
            
        }

        [TestMethod]
        public void TestFetchSubscriptionsWithPlan()
        {
            var plans = Plan.Fetch();
            Assert.IsInstanceOfType(plans, typeof(IList<Plan>));
            Assert.IsTrue(plans.Count > 0);
            var plan = plans[0];
            var subscriptions = plan.FetchSubscriptions();
            var subs = Subscription.Fetch(plan.Id);
            Assert.IsInstanceOfType(subscriptions, typeof(IList<Subscription>));
            Assert.IsTrue(subscriptions.Count > 0);
            Assert.IsInstanceOfType(subs, typeof(IList<Subscription>));
            Assert.IsTrue(subs.Count > 0);
            Assert.AreEqual(subscriptions.Count, subs.Count);

        }


        [TestMethod]
        public void TestPayoutThrift()
        {
            var plans = Plan.Fetch();
            Assert.IsInstanceOfType(plans, typeof(IList<Plan>));
            Assert.IsTrue(plans.Count > 0);
            var plan = plans[0];
            var response = plan.Payout();
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(200, response.StatusCode);
        }


        [TestMethod]
        public void TestCreateSaving()
        {
            var saving = Saving.Create("C# Library", 3, 30, new Interest(5.5, Interest.InterestFrequency.Every_Four_Weeks),
                true, 1.5);
            Assert.IsInstanceOfType(saving, typeof(Saving));
            Assert.IsTrue(saving.Id > 0);
        }


        [TestMethod]
        public void TestFetchSavings()
        {
            var savings = Saving.Fetch();
            Assert.IsInstanceOfType(savings, typeof(IList<Saving>));
            Assert.IsTrue(savings.Count > 0);
        }


        [TestMethod]
        public void TestGetSaving()
        {
            var saving = Saving.Get(27);
            Assert.IsInstanceOfType(saving, typeof(Saving));
            Assert.IsTrue(saving.Id == 27);
        }


        [TestMethod]
        public void TestCashoutSaving()
        {
            var saving = Saving.Get(27);
            Assert.IsInstanceOfType(saving, typeof(Saving));
            Assert.IsTrue(saving.Id == 27);
            var response = saving.Cashout(3, 5000, Cashout.Cash);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(300, response.StatusCode);
        }



        [TestMethod]
        public void TestSavingsDepositViaAccount()
        {
            var saving = Saving.Get(27);
            Assert.IsInstanceOfType(saving, typeof(Saving));
            Assert.IsTrue(saving.Id == 27);
            var account = new BankAccount("0236007136", "035");
            var response = saving.Deposit(3000, "test_ip", "c#fingerprint", account);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(202, response.StatusCode);
        }


        [TestMethod]
        public void TestValidateSavings()
        {
            var response = Saving.Validate("GPP39752081520190415T", "12345");
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreNotEqual(202, response.StatusCode);
        }
    }
}
