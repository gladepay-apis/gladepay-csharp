﻿using System;
using System.Collections.Generic;
using Gladepay.Investment;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    [TestClass]
    public class LoanTest
    {
        [TestMethod]
        public void TestRequestLoan()
        {
            var loan = Loan.Request(28, 40000, new Interest(3.5, Interest.InterestFrequency.Annually), true, 4.5, 20);
            Assert.IsNull(loan);
        }


        [TestMethod]
        public void TestFetchLoan()
        {
            var loans = Loan.Fetch();
            Assert.IsNotNull(loans);
            Assert.IsInstanceOfType(loans, typeof(IList<Loan>));
            Assert.IsTrue(loans.Count > 0);
        }


        [TestMethod]
        public void TestGetLoan()
        {
            var loan = Loan.Get(10);
            Assert.IsNotNull(loan);
            Assert.IsInstanceOfType(loan, typeof(Loan));
            Assert.AreEqual(loan.Id, 10);
        }
    }
}
