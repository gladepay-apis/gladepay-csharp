﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Gladepay;
using Gladepay.Payment;
using System.Net;
using System.Collections.Generic;
using Gladepay.Models;

namespace Test
{
    [TestClass]
    public class PaymentTest
    {
        [ClassInitialize]
        public static void TestInit(TestContext context)
        {
            Api.Init("GP0000001", "123456789");
        }

        [TestMethod]
        public void TestInitiateCardPayment()
        {
            var card = new Card("5438898014560229", 9, 19, "789", "3310");
            var user = new User("Shaibu","Shaibu","tellshaibu@gmail.com", "192.168.33.10", "cccvxbxbxb");
            var response = CardPayment.Initiate(user, card, 5000);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(202, response.StatusCode);

        }

        [TestMethod]
        public void TestChargeCardPayment()
        {
            var card = new Card("5438898014560229", 9, 19, "789", "3310");
            var user = new User("Shaibu", "Shaibu", "tellshaibu@gmail.com", "192.168.33.10", "cccvxbxbxb");
            var response = CardPayment.Charge(user, card, 5000, "GP392634596T", "PIN");
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(202, response.StatusCode);
        }


        [TestMethod]
        public void TestValidateCardPayment()
        {
            var card = new Card("5438898014560229", 9, 19, "789", "3310");
            var user = new User("Shaibu", "Shaibu", "tellshaibu@gmail.com", "192.168.33.10", "cccvxbxbxb");
            var response = CardPayment.Initiate(user, card, 40000);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual("otp", ((string)response.Json["apply_auth"]).ToLower());
            var txnRef = (string)response.Json["txnRef"];
            var otp = "123456";
            var validate_response = CardPayment.Validate(txnRef, otp);
            Assert.IsNotNull(validate_response);
            Assert.AreEqual(HttpStatusCode.OK, validate_response.Status);
            Assert.AreEqual(200, validate_response.StatusCode);
        }


        [TestMethod]
        public void TestCardTokenPayment()
        {
            var cardToken = "mvu1jumu";
            var user = new User("Shaibu", "Shaibu", "tellshaibu@gmail.com", "192.168.33.10", "cccvxbxbxb");
            var response = CardPayment.TokenCharge(user, 30000, cardToken);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(200, response.StatusCode);
        }


        [TestMethod]
        public void TestVerifyPayment()
        {
            var txnRef = "GPP65587495020190408M";
            var response = CardPayment.Verify(txnRef);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(200, response.StatusCode);
            Assert.AreEqual(txnRef, (string)response.Json["txnRef"]);
            Assert.AreEqual("successful", (string)response.Json["txnStatus"]);
        }

        [TestMethod]
        public void TestInitiateInstallmentalCardPayment()
        {
            var card = new Card("5438898014560229", 9, 19, "789", "3310");
            var user = new User("Shaibu", "Shaibu", "tellshaibu@gmail.com", "192.168.33.10", "cccvxbxbxb");
            var ps1 = new PaymentSchedule(new DateTime(2019, 06, 30, 0, 0, 0), 20);
            var ps2 = new PaymentSchedule(new DateTime(2019, 07, 30, 0, 0, 0), 20);
            var ps3 = new PaymentSchedule(new DateTime(2019, 08, 30, 0, 0, 0), 20);
            var ps4 = new PaymentSchedule(new DateTime(2019, 09, 30, 0, 0, 0), 20);
            var pss = new List<PaymentSchedule> { ps1, ps2, ps3, ps4 };
            var installment = new Installment(pss, 50000);
            var response = CardPayment.Initiate(user, card, 10000, installment:installment);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(202, response.StatusCode);

        }

        [TestMethod]
        public void TestInitiateInvalidInstallmentalCardPayment()
        {
            var card = new Card("5438898014560229", 9, 19, "789", "3310");
            var user = new User("Shaibu", "Shaibu", "tellshaibu@gmail.com", "192.168.33.10", "cccvxbxbxb");
            var ps1 = new PaymentSchedule(new DateTime(2019, 06, 30, 0, 0, 0), 20);
            var ps2 = new PaymentSchedule(new DateTime(2019, 07, 30, 0, 0, 0), 20);
            var ps3 = new PaymentSchedule(new DateTime(2019, 08, 30, 0, 0, 0), 20);
            var ps4 = new PaymentSchedule(new DateTime(2019, 09, 30, 0, 0, 0), 20);
            var pss = new List<PaymentSchedule> { ps1, ps2, ps3, ps4 };
            var installment = new Installment(pss, 50000);
            Assert.ThrowsException<Exception>(() =>
            {
                CardPayment.Initiate(user, card, 10000.50, installment: installment);
            });
        }

        [TestMethod]
        public void TestInitiateRecurrentCardPayment()
        {
            var card = new Card("5438898014560229", 9, 19, "789", "3310");
            var user = new User("Shaibu", "Shaibu", "tellshaibu@gmail.com", "192.168.33.10", "cccvxbxbxb");
            var recurrent = new CardPayment.Recurrent(CardPayment.RecurringFrequency.DAILY, 22);
            var response = CardPayment.Initiate(user, card, 10000, recurrent: recurrent);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(202, response.StatusCode);
        }

        [TestMethod]
        public void TestCardTokenRecurrentPayment()
        {
            var cardToken = "mvu1jumu";
            var user = new User("Shaibu", "Shaibu", "tellshaibu@gmail.com", "192.168.33.10", "cccvxbxbxb");
            var recurrent = new CardPayment.Recurrent(CardPayment.RecurringFrequency.DAILY, 22);
            var response = CardPayment.TokenCharge(user, 30000, cardToken, recurrent:recurrent);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(200, response.StatusCode);
        }

        [TestMethod]
        public void TestBankChargePayment()
        {
            var user = new User("Shaibu", "Shaibu", "tellshaibu@gmail.com", "192.168.33.10", "cccvxbxbxb");
            var account = new BankAccount("0236007136", "035");
            var response = AccountPayment.Charge(user, account, 2500);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(202, response.StatusCode);
        }

        [TestMethod]
        public void TestBankInstallmentalCharge()
        {
            var user = new User("Shaibu", "Shaibu", "tellshaibu@gmail.com", "192.168.33.10", "cccvxbxbxb");
            var ps1 = new PaymentSchedule(new DateTime(2019, 06, 30, 0, 0, 0), 20);
            var ps2 = new PaymentSchedule(new DateTime(2019, 07, 30, 0, 0, 0), 20);
            var ps3 = new PaymentSchedule(new DateTime(2019, 08, 30, 0, 0, 0), 20);
            var ps4 = new PaymentSchedule(new DateTime(2019, 09, 30, 0, 0, 0), 20);
            var pss = new List<PaymentSchedule> { ps1, ps2, ps3, ps4 };
            var installment = new Installment(pss, 50000);
            var account = new BankAccount("0690000007", "044");
            var response = AccountPayment.Charge(user, account, 10000, installment:installment);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(202, response.StatusCode);
        }

        [TestMethod]
        public void TestValidateBankCharge()
        {
            var user = new User("Shaibu", "Shaibu", "tellshaibu@gmail.com", "192.168.33.10", "cccvxbxbxb");
            var ps1 = new PaymentSchedule(new DateTime(2019, 06, 30, 0, 0, 0), 20);
            var ps2 = new PaymentSchedule(new DateTime(2019, 07, 30, 0, 0, 0), 20);
            var ps3 = new PaymentSchedule(new DateTime(2019, 08, 30, 0, 0, 0), 20);
            var ps4 = new PaymentSchedule(new DateTime(2019, 09, 30, 0, 0, 0), 20);
            var pss = new List<PaymentSchedule> { ps1, ps2, ps3, ps4 };
            var installment = new Installment(pss, 50000);
            var account = new BankAccount("0690000007", "044");
            var response = AccountPayment.Validate("GPP29253635520190409X", "123456");
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(202, response.StatusCode);
        }

        [TestMethod]
        public void TestInitiateSplitCard()
        {
            var user = new User("Shaibu", "Shaibu", "tellshaibu@gmail.com", "192.168.33.10", "cccvxbxbxb");
            var card = new Card("5438898014560229", 9, 19, "789", "3310");
            var split1 = new Split("201sdaas30s", percentage: 40);
            var split2 = new Split("etr01sdyt2", fixedShare: 100);
            var splits = new List<Split> { split1, split2 };
            var response = CardPayment.Initiate(user, card, 10000, split: splits);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(202, response.StatusCode);
        }

        [TestMethod]
        public void TestSplitAccountPayment()
        {
            var user = new User("Shaibu", "Shaibu", "tellshaibu@gmail.com", "192.168.33.10", "cccvxbxbxb");
            var account = new BankAccount("0690000007", "044");
            var split1 = new Split("201sdaas30s", percentage: 40);
            var split2 = new Split("etr01sdyt2", fixedShare: 100);
            var splits = new List<Split> { split1, split2 };
            var response = AccountPayment.Charge(user, account, 10000, split: splits);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(202, response.StatusCode);
        }


        [TestMethod]
        public void TestGetTransactionFee()
        {
            var response = CardPayment.GetTransactionFee("5438898014560229", 20000);
            Assert.AreEqual(380, response);
        }


        [TestMethod]
        public void TestGhanaMobileCharge()
        {
            var user = new User("Shaibu", "Shaibu", "tellshaibu@gmail.com", "192.168.33.10", "cccvxbxbxb");
            var account = new BankAccount("0236007136", "035");
            var response = MobileMoney.GhanaCharge(user, "054709929220", "MTN", 15000);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(202, response.StatusCode);
        }

        [TestMethod]
        public void TestMpesaMobileCharge()
        {
            var user = new User("Shaibu", "Shaibu", "tellshaibu@gmail.com", "192.168.33.10", "cccvxbxbxb");
            var account = new BankAccount("0236007136", "035");
            var response = MobileMoney.MpesaCharge(user, "0926420185", 20000);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(202, response.StatusCode);
        }

        [TestMethod]
        public void TestPagaMobileCharge()
        {
            var user = new User("Shaibu", "Shaibu", "tellshaibu@gmail.com", "192.168.33.10", "cccvxbxbxb");
            var account = new BankAccount("0236007136", "035");
            var response = MobileMoney.PagaCharge(user, "mytest.com", 30000);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(202, response.StatusCode);
        }

        [TestMethod]
        public void TestQrPay()
        {
            var user = new User("Shaibu", "Shaibu", "tellshaibu@gmail.com", "192.168.33.10", "cccvxbxbxb");
            var response = QRPay.Charge(user, "gladepaytest", 2500);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(202, response.StatusCode);
        }

        [TestMethod]
        public void TestUssdCharge()
        {
            var user = new User("Shaibu", "Shaibu", "tellshaibu@gmail.com", "192.168.33.10", "cccvxbxbxb");
            var response = USSD.Charge(user, "0902620185", USSD.Bank.GTB, "0034569876", 3000);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(202, response.StatusCode);
        }


        [TestMethod]
        public void TestPayWithPhone()
        {
            var user = new User("Shaibu", "Shaibu", "tellshaibu@gmail.com", "192.168.33.10", "cccvxbxbxb");
            var response = PhonePay.Charge(user, "0902620185", 5000);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(202, response.StatusCode);
        }
    }
}
