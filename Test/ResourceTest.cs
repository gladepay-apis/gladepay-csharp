﻿using System;
using System.Net;
using Gladepay;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Test
{
    [TestClass]
    public class ResourceTest
    {
        [TestMethod]
        public void TestFetchChargeableBanks()
        {
            var banks = Bank.ChargeableBanks();
            Assert.AreNotEqual(0, banks.Count);
        }

        [TestMethod]
        public void TestVerifyAccountName()
        {
            var response = new Bank("044").VerifyAccountName("0040000009");
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual("success", response.StatusMessage);
        }

        [TestMethod]
        public void TestVerifyBvn()
        {
            var response = Bank.VerifyBvnNumber("12345678901");
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual("success", response.StatusMessage);
        }
    }
}
