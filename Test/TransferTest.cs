﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Gladepay;
using System.Net;
using System.Collections.Generic;

namespace Test
{
    [TestClass]
    public class TransferTest
    {   
        [TestMethod]
        public void TestSingleTransferWithDefaultApi()
        {
            var ct = getCurrentTime();
            var data = new Transfer.TransferData(20000.50, "058", "0040143898", "gladepay c# library",
                $"gcl-{ct.ToString()}-oref", "Test transfer from Gladepay c# library");
            var response = Transfer.SingleTransfer(data);
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(200, response.StatusCode);
            Assert.IsNotNull((string)response.Json["txnRef"]);
        }


        [TestMethod]
        public void TestBulkTransferWithDefaultApi()
        {
            var ct = getCurrentTime();
            var data1 = new Transfer.TransferData(20000.50, "058", "0040143898", "gladepay c# library",
                $"gcl-{ct.ToString()}-oref1", "Test transfer from Gladepay c# library");
            var data2 = new Transfer.TransferData(20000.50, "058", "0040143898", "gladepay c# library",
                $"gcl-{ct.ToString()}-oref2", "Test transfer from Gladepay c# library");
            var data3 = new Transfer.TransferData(20000.50, "058", "0040143898", "gladepay c# library",
                $"gcl-{ct.ToString()}-oref3", "Test transfer from Gladepay c# library");
            var response = Transfer.BulkTransfer(new List<Transfer.TransferData>()
            {
                data1, data2, data3
            });
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(200, response.StatusCode);
            Assert.IsNotNull((string)response.Json["txnRef"]);
        }

        [TestMethod]
        public void TestVerifyTransferWithDefaultApi()
        {
            var response = Transfer.VerifyTransfer("gcl-1554807282374-oref");
            Assert.IsNotNull(response);
            Assert.AreEqual(HttpStatusCode.OK, response.Status);
            Assert.AreEqual(200, response.StatusCode);
            Assert.IsNotNull((string)response.Json["txnRef"]);
        }


        public long getCurrentTime()
        {
            return (long)DateTime.Now.Subtract(DateTime.MinValue.AddYears(1969)).TotalMilliseconds;

        }
    }
}
